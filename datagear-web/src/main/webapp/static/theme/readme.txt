common.css
由公用样式（common.css）和主题样式（[theme]/common.css）组成，避免维护多套；

jQuery-File-Upload-9.21.0
不存在主题样式

jquery.layout-1.4.0
主题样式已在各主题的common.css中定义

jstree-3.3.4
主题样式已在各主题的common.css中定义

DataTables-1.10.15
主题样式已在各主题的common.css中定义

jquery-ui-1.12.1
没有公用样式，主图样式由官网生成
